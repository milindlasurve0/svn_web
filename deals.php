<?php
session_start();

if ($_SESSION["flag"] != "true") {

    header("location:login.php?logout=logout");
}
include 'config.php';
include 'common.php';
//include 'function.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Deals || PAY1</title>
        <meta charset="utf-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />


<!--        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
         <link rel="stylesheet" type="text/css" href="css/global.css">
        <![endif]-->
         <link rel="stylesheet" type="text/css" href="css/global.css">
    </head>
    <body>
        <div>
            <!-- navigation -->
            <!--<div class="mobilemenu">
                    <p class="mobiletoggle" data-idd="#mobnav"></p>
                    <div class="walletTOP">
                      <ul class="nav nav-pills dealtoplist">
                            <li>
                                    <span class="dropdown-toggle" data-toggle="dropdown">
                                              <table>
                                                    <tr>
                                                            <td><span class="deals-balance"></span></td>
                                                            <td>Balance<br> <strong>60,000 <span class="caret"></span></strong></td>
                                                    </tr>
                                              </table>
                                    </span>
                                    <ul class="dropdown-menu" role="menu">
                      <li>1,00,000</li>
                      <li>2,00,000</li>
                      <li>3,00,000</li>
                    </ul>
                              </li>
                            </ul>
                    </div>
                    <a href="index1.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
            </div>-->

<!--            <div class="leftnavi" id="mobnav">
                <ul>
                    <li>
                        <a href="index.php?mobile"><img src="images/logo.jpg"></a>
                        <strong>Recharge</strong>
                    </li>
                    <li><a href="index.php?mobile"  class="current"><span class="customicon-mobile"></span> Mobile</a></li>
                    <li><a href="index.php?dth"><span class="customicon-dth"></span> DTH</a></li>
                    <li><a href="index.php?data"><span class="customicon-dcable"></span> Data Card</a></li>
                    <li class="freespace">&nbsp;</li>
                    <li><a href="deals.php"><span class="customicon-deals"></span>Deals</a></li>
                    <li class="bill freespace"><small>Bill</small></li>
                    <li><a href="index.php?postpaid"><span class="customicon-pmobile"></span> Postpaid mobile</a></li>
                    <li><a href="#electricity" data-target=".ordersuccess" data-toggle="modal"><span class="customicon-Electricity"></span> Electricity</a></li>
                </ul>
            </div>-->
<?php include 'left.php';  ?>

           
            <!-- //close navigation -->
            <div class="shiftbox">
                <div class="hutpart">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
<?php include 'header.php';
//sinclude 'analyticstracking.php'; 
?>
                            </div>
                        </div>
                        <div class="row mT20">
                            <div class="col-sm-3">
                                <div class="menuMOBILESET">
                                    <ul id="myTab" class="nav nav-tabs1 dealslist" role="tablist">
                                        <li class="active">
                                            <a data-toggle="tab" role="tab" href="#tab1">My Gifts</a>
                                        </li>
                                     							<li class="">
                                                                                            <a data-toggle="tab" role="tab" onclick="getalldeals()" href="#tab2">Available Free Gifts</a>
                                                                                                </li>
                                                                                                   <!--<li class="">
                                                                                                        <a data-toggle="tab" role="tab" href="#tab3">Available</a>
                                                                                                </li>
                                                                                                <li class="">
                                                                                                        <a data-toggle="tab" role="tab" href="#tab4">Expired</a>
                                                                                                </li>-->
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-9">
                                <div id="ListTabContent" class="tab-content clearfix">
                                    <div class="dealsbread">
                                        <ul>
                                            <li></li>
                                            <li><a href="#">Free Gifts</a></li>
                                            <!--<li>&raquo;</li>-->
                                            <!--<li><span></span></li>-->
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div id="tab1" class="tab-pane fade active in">
                                        <div class="dealfullbox" id="mydeal">


                                        </div>
                                    </div>
                                    <div id="tab2" class="tab-pane fade">
                                        <form method ='post' id='deal_form' action='wallet.php'>
                                            <input type='hidden' name='lat' id='lat' value=''>
                                            <input type='hidden' name='shop_id' id='shop_id' value=''>
                                            <input type='hidden' id='long' name='long' value=''>
                                        </form>
                                         <div id="container">
                                         <div id="slides">
                                <div class="slides_container">
                                    
                                </div>
                                <a href="#" class="prev"><img src="images/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
				<a href="#" class="next"><img src="images/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
                                </div>
                                        
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- footer box -->
                <?php include 'footer.php'; ?>

            </div><!-- //shiftbox -->
        </div>
        <script src="js/slides.min.jquery.js"></script>
        <script>
         function slideimage(){      
			var startSlide = 1;
			// Get slide number if it exists
			if (window.location.hash) {
				startSlide = window.location.hash.replace('#','');
			}
			// Initialize Slides
			$('#slides').slides({
				preload: true,
				preloadImage: 'images/loading.gif',
				generatePagination: true,
				play: 5000,
				pause: 100,
				hoverPause: true,
				// Get the starting slide
				start: startSlide,
				animationComplete: function(current){
					// Set the slide number as a hash
					window.location.hash = '#' + current;
				}
			});
                    }
                    </script>
        <script type="text/javascript">
            $(document).ready(function() {

                var serviceurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/get_my_deal/?";
                var html = '';
                var i = 1;
                $.ajax({
                    url: serviceurl,
                    type: "GET",
                    data: {
                        freebie: "true",
                        res_format: "jsonp"
                    },
                    timeout: 50000,
                    dataType: "jsonp",
                    jsonpCallback: 'callback',
                    crossDomain: true,
                    success: function(dealsdata) {
                        if (dealsdata.status == "success")
                        {
                            $.each(dealsdata.description, function(dealkey, dealvalue) {
                                html += "<div class='dealboxx'>";
                                html += "<div class='media ddll'><dl class='dl-horizontal'>";
                                html += "<dt><img class='media-object' src='" + dealvalue.img_url + "' alt=''></dt>";
                                html += "<dd><h4 class='media-heading' data-toggle='collapse' data-parent='#accordion-" + i + "' data-target='#collapse-" + i + "'>" + dealvalue.deal_name + "</h4>";
                                html += "<div class='col-md-7 leftrightpad mT10 statustext'><p><strong>Coupon status : </strong><span>" + dealvalue.coupon_status + "</span></p><p><strong>Expires : </strong><span>" + dealvalue.expiry + "</span></p></div>";
                                html += "<div class='col-md-5 leftrightpad mT10'><div class='dealslinksLR'>";
                                html += "</div>";
                                html += "</div></dd></dl></div>";
                                html += "<div class='panel-group' id='accordion-" + i + "'>";
                                html += " <div id='collapse-" + i + "' class='panel-collapse collapse'><div class='col-md-12 leftrightpad'>";
                                html += "<h4 class='dealshead'>" + dealvalue.deal_name + "</h4><small class='dealtime'>" + dealvalue.expiry + "</small>";
                                html += "<table class='borderBOTTOM'><tr><th>Deal Code</th><th>&nbsp;</th><th>Quantity Purchased</th></tr><tr><td><p>" + dealvalue.code + "<p></td><td><p>Rs.&nbsp;" + dealvalue.amount + "</p></td><td><p>" + dealvalue.quantity + "</p></td></tr></table>";
                                html += "<table class='borderBOTTOM'><tr><th>Mode of Payment</th><th>Tranaction ID</th></tr><tr><td><p>" + dealvalue.transaction_mode + "</p></td><td><p>" + dealvalue.transaction_id + "</p></td></tr></table></div>";
                                html + "</div></div>"
                                html += "</div>";
                                //console.log(html);
                                $("#mydeal").append(html);
                                html = '';
                                i++;

                            });

                        }
                        else if (dealsdata.errCode == "201") {
                            window.location = "login.php?logout=";
                        }

                    },
                    error: function(error) {
                    }
                });

            });
            
     function getalldeals()
      {
      $(".slides_container").html('');
      var serviceurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/get_all_deal_data/?";
      var latitude = "<?php echo $_SESSION["latitude"]?>";
      var longitude = "<?php echo $_SESSION["longitude"]?>";
      var html = "";
      
         $.ajax({
            url: serviceurl,
            type:"GET",
            data:{
                 latitude : latitude,
                 longitude: longitude,
                 res_format : "jsonp"
                 },
          
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(dealsdata){
              if(dealsdata.status=="success")
              {  
                 i=1;
                 $.each(dealsdata.description,function(dealkey,dealvalue){
                    // console.log(dealvalue.offer_detail);
                          var imageurl = dealvalue.img_url;
                           var deal_id  = "\'" + dealvalue.id + "\'";
                          html+="<div class='slide'><b><span class=''>"+dealvalue.offer_detail.name+"</span></b><b><p class=''>"+dealvalue.offer_detail.short_desc+"</p></b><div class=''><div class=''><img class='' height='300px;' width='300px;' src='"+imageurl+"'>"
                          html+="<ul class='hutLIST clearfix'>"
                           html+="<li style='color:red;'><small><span style='align:center;'><b>"+i+"     of "+dealsdata.description.length+"</b></small></span></li>"
                          html+="</ul><h2>"+dealvalue.content_txt.replace('&lt','<').replace('&gt', '>')+"</h2></div></div>"
                          html+="<div class='col-sm-6 row_left'>"
                          html+="<h3>"+dealvalue.location_detail[0].address+"</h3>"
                          html+="</div>";
                          html+="<div class='col-sm-4 row_right'>"
                          html+='<a href="javascript:getdeallocation('+dealvalue.latitude+','+dealvalue.longitude+','+deal_id+')"><img  src="images/map-icon.png"></a>'
                          if(latitude!='' && longitude!=''){
                          html+="<h3>"+Number(dealvalue.distance).toFixed(2)+"</h3>"
                          if(dealvalue.distance>1){
                               html+="<span>(Km)</span>"
                            } else {
                             html+="<span>(m)</span>"
                            }
                            }
                          html+="</div>";
                          html+="<div style='align:center;'><a href='index.php'><button class='common-btn-submit'>Recharge Now</button></div></a>"
                          html+="</div>"
				
				
                 i++;   
              });
				
             $(".slides_container").append(html);
             slideimage();
            // $(".slides_container").html('');
           
             
              }},
            error: function (error) {
            }
            });
    
}
        </script>
    </body>
</html>

          <!-- <a href='#'><span class='dealslinks-qr'></span></a><a href='deals.php'><span class='dealslinks-phone'></span></a><a href='#'><span class='dealslinks-msg'></span></a>!--->                                                            