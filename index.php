
<?php

 session_start(); 

//require_once 'function.php';
require_once 'config.php';
require_once 'common.php';

$getcircle = file_get_contents("circle.json");
$getcircle = json_decode($getcircle);
$getcircle = objectToArray($getcircle);

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
   $method = $_POST["method"];
   $mobileno = $_POST["mobileno"];
   $curl_url = PANEL_URL."/apis/receiveWeb/mindsarray/mindsarray/json?method=$method&mobile=$mobileno";
   $res = getCurlAjaxRespose($curl_url);
    $res = trim($res);
    $res = trim($res, ")");
    $res = trim($res, "(");
    $res = trim($res, ";");
    $res = substr_replace($res, "", -1);
    echo $res;
    die;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home || PAY1</title>
	<meta charset="utf-8">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>
        <link rel="stylesheet" type="text/css" href="css/global.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
      div.scroll {
   // background-color: #00FFFF;
    width: 650px;
    height: 300px;
    overflow-y: scroll;
}

.slides_container1 {
			width:1200px;
                        height:320px;
			display:none;
		}

		.slides_container1 div.slide1 {
			width:1200px;
                        height:320px;
			display:block;
                        // border:1px solid;
		}

		
		.item2 {
			float:left;
			width:350px;
			height:320px;
			margin:0 10px;
                      //  border:1px solid;
			//background:#efefef;
		}

		.pagination {
			list-style:none;
			margin:0;
			padding:0;
		}

		.pagination .current a {
			color:red;
		}
  </style>

</head>

<body onload="getalldeals();">
        
<div>
<img src="images/MainPageImage.jpg" class="homebgIMG">
<!-- navigation -->
<div class="mobilemenu">
 	<p class="mobiletoggle" data-idd="#mobnav"></p>
	<a href="index.php" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
</div>

<div class="leftnavi" id="mobnav">
	<ul class="mobile">
		<li>
                   <a href="#index.php"><img src="images/logo.jpg"></a>
			<strong>Recharge</strong>
		</li>
		<li ><a href="#mobile" data-dismiss="modal" class="mobile current"><span class="customicon-mobile"></span> Mobile</a></li>
		<li><a href="#dth" data-dismiss="modal"  class="dth"><span class="customicon-dth"></span> DTH</a></li>
		<li><a href="#data" data-dismiss="modal"  class="data"><span class="customicon-dcable"></span> Data Card</a></li>
		<li class="freespace">&nbsp;</li>
                <li><a href="#deals.php" class=""><span class="customicon-deals"></span>Free Gifts</a></li>
<!--		<li class="bill freespace"><small>Bill</small></li>
		<li><a href="#postpaid" class="postpaid"><span class="customicon-pmobile"></span> Postpaid mobile</a></li>-->
<!--		<li><a href="#electricity"  data-target=".ordersuccess" data-toggle="modal"><span class="customicon-Electricity"></span> Electricity</a></li>-->    
               
	</ul>
</div>

<div id="info"></div>
<!--<table>
<tr data-toggle="modal" data-target=".congratulations">
                                                <td>Rs. 145</td>
                                                <td>Rs. 145</td>
                                                <td>30 Days</td>
                                                <td>Full Talk Time</td>
                                            </tr>
</table>-->
<div class="shiftbox">
	<div class="container">
		<div class="row">
                    <div class="col-sm-12">
                    
			<?php  include 'header.php';
                               include 'analyticstracking.php';
                               include 'index-js.php';
                        ?>
                    </div>
                    
		</div>
            <div class="col-sm-12" id="setrechargetype"><input type="radio" id="prepaid_type" name="prepaid_type" checked="checked" onclick="setRechargeType('Prepaid');" id="prepaid_type"><label for="prepaid_type">Prepaid</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" id="postpaid_type" onclick="setRechargeType('Postpaid');" name="prepaid_type" id="postpaid_type"><label for="postpaid_type">Postpaid</label></div>
		<div class="row" id="mobile">
                    <form method="post" id="mob_form" action="login.php">
                        <input type="hidden" id="mob_flag" name="mob_flag" value="1">
                        <input type="hidden" id="mob_circle" name="mob_circle" value="<?php echo $_POST["rec_circle"] ?>">
                        <input type="hidden" id="mob_max" name="mob_max" value="<?php echo $_POST["rec_max"]; ?>">
                        <input type="hidden" id="mob_min" name="mob_min" value="<?php echo $_POST["rec_min"]; ?>">
                         <input type="hidden" id="mob_id" name="mob_id" value="<?php echo $_POST["rec_id"]; ?>">
                          <input type="hidden" id="mob_product" name="mob_product" value="<?php echo $_POST["rec_product"]?>">
			<div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
                        
<!--			<div class="col-sm-12"><h2 class="mobilerecharge">Prepaid Mobile</h2></div>-->
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Pre-paid Mobile Number</label>
                                        <input type="text" autocomplete="off" placeholder="Enter Prepaid Mobile Number" class="form-control" name="mob_number" <?php if($_POST["rec_type"]=="1"){ ?>value="<?php echo $_POST["rec_number"]; ?>" <?php } ?>id="mob_number" maxlength="10" onkeypress="getoperatorvalue(this.value,'mob_provider','mob_amount');return isNumberKey(event)" onchange="getoperatorvalue(this.value,'mob_provider','mob_amount');return isNumberKey(event)" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
					<select class="form-control" name="mob_provider" id="mob_provider" onchange="getplanoperatorchange('mob')">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["Mobile"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="1"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>
 
                            
                            
                             <div class="form-group" id="spl_rchg" style="display: none;">
						  	<div><label class="" for="">Special Recharge</label></div>
						  	<div class="btn-group" data-toggle="buttons">
								  <label class="btn btn-primary">
                                                                      <input type="radio" name="special_recharge" value="1" id="spl_rchg_yes">Yes
								  </label>
								  <label class="btn btn-primary">
                                                                      <input type="radio" name="special_recharge" value="0" id="spl_rchg_no">No
								  </label>
								</div>
                                                        
						  </div>

				<div class="inputfild">
					<label>Amount</label>
                                        <input type="text" placeholder="Enter Amount" class="form-control" <?php if($_POST["rec_type"]=="1"){ ?>value="<?php echo $_POST["rec_amount"];?>"<?php } ?> name="mob_amount" id="mob_amount"  onkeypress="return isNumberKey(event)">
                                        <span  class="opencheckplans"  id="mob_checkplan"  style="display:none;" >Check Plan</span>
					
				</div>
<!--onclick="return validate('mob_number','mob_provider','mob_amount');"-->
				<div class="inputfild">
					<button type="button" onclick="orderclick('mob');" data-toggle="modal"  data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed to Recharge</button>
				</div>
			</div>
			<div class="col-sm-6">
<!--				<div class="welcome">

				</div>-->
<div class="checkplans" id="showmobplan">
                                              <div class="sliderowbar">
						<ul class="nav nav-tabs" role="tablist" id ="mob_plan">
						
						</ul>
                                              </div>
                                            <table class="form-control" id="mobile_circle" style="display:none;border: none;">
                                                
                                                <tr width="673">
                                                    <th>Select Circle</th>
                                                    
                                                    <td><select class="form-control" name="mob_cir" id="mob_cir" onchange="getplanoperatorchange('mob')">
						<option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>
						
					</select></td>
                                                </tr>
                                            </table>

						<!-- Tab panes -->
						<div class="tab-content mT20"  id ="mob_plandetails">
    
						</div>
						<!-- close -->
<!--						<div class="text-right">
							<a href="javascript:void(0)" class="hidecheckplans" title="Close">Close &nbsp;</a>
						</div>						-->
					</div>
			</div>
                    </form>
		</div>

                                     <div class="loader">
                                                <center>
                                                    <img class="loading-image" src="images/ajax-loader.gif" alt="loading..">
                                                </center>
                                            </div>
            <div id="dth" class="row" style="display:none;">
                <form method="post" id="dth_form" action="login.php">
               <input type="hidden" name="dth_flag" id="dth_flag" value="2">
               <input type="hidden" name="dth_max" id="dth_max" value="<?php echo $_POST["rec_max"]; ?>">
                <input type="hidden" name="dth_min" id="dth_min" value="<?php echo $_POST["rec_min"]; ?>">
                <input type="hidden" name="dth_id" id="dth_id" value="<?php echo $_POST["rec_id"]; ?>">
                 <input type="hidden" name="dth_product" id="dth_product" value="<?php echo $_POST["rec_product"]; ?>">
                  <input type="hidden" name="dth_circle" id="dth_circle" value="<?php echo $_POST["rec_circle"]; ?>">
                
                <div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">DTH </h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
                            
                            <div class="inputfild">
					<label></label>
					<select class="form-control" name="dth_provider" id="dth_provider" onchange="getplanoperatorchange('dth');">
						<option value="">Select Operator</option>
                                                <?php foreach($arrayval["dth"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="2"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>
				<div class="inputfild">
					<label></label>
                                        <input type="text" class="form-control" <?php if($_POST["rec_type"]=="2"){ ?> value="<?php echo $_POST["rec_number"]; ?>" <?php }?>onkeypress="return isNumberKey(event)" name="dth_number" id="dth_number" placeholder="">
				</div>
                            
                            
				

				<div class="inputfild">
					<label>Amount</label>
                                        <input type="text"  name="dth_amount" placeholder="Enter Amount" id="dth_amount"  <?php if($_POST["rec_type"]=="2"){ ?>value="<?php echo $_POST["rec_amount"] ?>" <?php } ?>onkeypress="return isNumberKey(event)" class="form-control" placeholder="" >
                                        <span  class="opencheckplans" id="dth_checkplan" style="display:none;">Check Plans</span>
                                       
					
				</div>

				<div class="inputfild">
					<button type="button" data-toggle="modal" onclick="orderclick('dth');" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed to Recharge</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="checkplans" id="showdthplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs" id="dth_plan" role="tablist">
						 
						</ul>
                                            </div>
						<!-- Tab panes -->
						<div class="tab-content mT20" id="dth_plandetails">
						 
						</div>
						<!-- close -->
						<div class="text-right">
                                                   <a href="javascript:void(0);" class="hidecheckplans"  title="Close">Close &nbsp;</a>
						</div>						
					</div>
			</div>
                </form>
            </div>
            
            <div id="data" style="display:none;">
                <form method="post" id="data_form" action="login.php">
                    <input type="hidden" name="data_id" id="data_id" value="<?php echo $_POST["rec_id"]; ?>">
                    <input type="hidden" name="data_max" id="data_max" value="<?php echo $_POST["rec_max"]; ?>">
                     <input type="hidden" name="data_min" id="data_min" value="<?php echo $_POST["rec_min"]; ?>">
                     <input type="hidden" id="data_flag" name="data_flag" value="3">
                       <input type="hidden" id="data_product" name="data_product" value="<?php echo $_POST["rec_product"]; ?>">
                      <input type="hidden" id="data_circle" name="data_circle" <?php if($_POST["rec_type"]=="3"){?>value="<?php echo $_POST["rec_circle"]; ?>" <?php }?>>
                
                <div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12"><h2 class="mobilerecharge">Data Card</h2></div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Data Card Number</label>
                                        <input type="text" placeholder="Enter Data Card Number" class="form-control" <?php if($_POST["rec_type"]=="3"){ ?>value="<?php echo $_POST["rec_number"] ?>" <?php }?>name="data_number" id="data_number" onkeypress="getdataoperatorvalue(this.value,'data_provider','data_amount')">
				</div>
                            
                            
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
					<select class="form-control" id="data_provider" name="data_provider" onchange="getplanoperatorchange('data')">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["data"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>"<?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="3"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>
                            
                           

				<div class="inputfild">
					<label>Amount</label>
                                        <input type="text" placeholder ="Enter Amount" name="data_amount"  <?php if($_POST["rec_type"]=="3"){ ?>value="<?php echo $_POST["rec_amount"] ?>" <?php }?> id="data_amount" class="form-control"  onkeypress="return isNumberKey(event)">
                                        <span class="opencheckplans" id="data_checkplan"  style="display: none;">check plan</span>
					
				</div>

				<div class="inputfild">
					<button type="button" data-toggle="modal" onclick="orderclick('data');" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed to Recharge</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="checkplans" id="showdataplan">
                                            <div class="sliderowbar">
						<ul class="nav nav-tabs"  id ="data_plan" role="tablist">
						  
						</ul>
                                            </div>

                                             <table class="form-control" id="dat_circle" style="display:none;border: none;">
                                                
                                                <tr>
                                                    <th style="border:none;">Select Circle</th>
                                                    <td style="border:none;"><select class="form-control" name="data_cir" id="data_cir" onchange="getplanoperatorchange('data')">
						<option value="">Select Circle</option>
                                                <?php foreach($getcircle as $key => $val){ if($val["code"]!="SC"){?>
                                                <option value="<?php echo $val["code"] ?>"><?php echo $val["name"] ?></option>
                                                <?php }} ?>
						
					</select></th>
                                                </tr>
                                            </table>

						<!-- Tab panes -->
						<div class="tab-content mT20" id="data_plandetails">
						  
						</div>
						<!-- close -->
						<div class="text-right">
							<a href="javascript:void(0)" class="hidecheckplans" title="Close">Close &nbsp;</a>
						</div>						
					</div>
			</div>
                </form>
            </div>
            <div class="row" id="postpaid" style="display: none;">
                <form method="post" id="post_form" action="login.php">  
                        <input type="hidden" name="post_flag" id="post_flag" value="4">
                         <input type="hidden" name="post_id" id="post_id" value="<?php echo $_POST["rec_id"]?>">
                         <input type="hidden" name="post_min" id="post_min" value="<?php echo $_POST["rec_min"]?>">
                         <input type="hidden" name="post_max" id="post_max" value="<?php echo $_POST["rec_max"]?>">
                         <input type="hidden" name="post_charge" id="post_charge" value="<?php echo $_POST["rec_charge"]?>">
                         <input type="hidden" name="post_servicecharge" id="post_servicecharge" value="">
			<div class="col-sm-12">
				<div class="welcome showbelow768hideUp768">
					<div class="">
						<p>Welcome to <span>Pay1</span> india's first cash recharge portal.</p>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
<!--                        <div class="col-sm-12">Prepaid<input type="radio" name="prepaid_type1"  onclick="setRechargeType('Prepaid');" id="">Postpaid<input type="radio" onclick="setRechargeType('Postpaid');" checked="checked" name="prepaid_type1" id=""></div>-->
<!--			<div class="col-sm-12"><h2 class="mobilerecharge">Postpaid Mobile </h2></div>-->
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="inputfild">
					<label>Post-paid Mobile Number</label>
                                        <input type="text" placeholder="Enter Postpaid Mobile Number" class="form-control" <?php if($_POST["rec_type"]=="4"){ ?> value="<?php echo $_POST["rec_number"];?>" <?php } ?> maxlength="10"  id="post_number" name="post_number" onkeypress="getdataoperatorvalue(this.value,'post_provider','post_amount');return isNumberKey(event)" placeholder="">
				</div>
                            
                            
				<div class="inputfild">
					<label>Service Provider</label>
                                        <select class="form-control" name="post_provider" id="post_provider" onchange="getplanoperatorchange('post');">
						<option value="">Select Service Provider</option>
                                                <?php foreach($arrayval["postpaid"] as $key => $val){?>
                                                <option value="<?php echo $val["opr_code"] ?>" <?php if($_POST["rec_provider"]==$val["opr_code"] && $_POST["rec_type"]=="4"){ ?> selected="selected"<?php }?>><?php echo $val["name"] ?></option>
                                                <?php } ?>
						
					</select>
				</div>

				<div class="inputfild">
					<label>Amount</label>
                                        <input type="text"  placeholder="Enter Amount"<?php if($_POST["rec_type"]=="4"){ ?> value="<?php echo $_POST["rec_amount"] ?>" <?php } ?> class="form-control" name="post_amount" id="post_amount" onkeypress="return isNumberKey(event)">

				</div>

				<div class="inputfild">
					<button type="button" id="postpaid" onclick="orderclick('post');"  data-toggle="modal" data-target=".orderconfirmation" class="btn btn-primary btn-lg btn-block">Proceed to Pay Bill</button>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="welcome">
					
				</div>
			</div>
                    </form>
		</div>
   
            </div>
	</div>

	<div class="whychoose">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6 text-center">
							<img src="images/why-choose.jpg">
						</div>
						<div class="col-sm-6">
							<div class="whytextbox">
								<h2>Why choose Pay1  over others?</h2>
								<p>
									Pay1 is an app that helps you make your wallet even lighter. It helps you carry rupees without actually carrying it. Bill payments and money transfer has never been that easy before. With a team focusing on delivering a safer, secured and easy experience Pay1 is one of the most powerful digital wallet one can own.
								</p>
								<a href="#">Read More &#x025B8;</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hutpart">
            <?php if(isset($_SESSION['flag'])){ ?>
        
		<div class="container">
			<div class="row" id ="slides1">
                            <div class="slides_container1">
                                
                            </div>

				
			</div>
		</div>
	
            <?php }?>
	</div>

<!-- popup for order confirmation -->
<div class=""  style="display:none;" id="orderconfirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
              <form method="post" action="" id="payment_form">
                  <input type="hidden" name="recharge_amount" id="recharge_amount" value="">
                    <input type="hidden" name="recharge_operator" id="recharge_operator" value="">
                    <input type="hidden" name="recharge_flag" id="recharge_flag" value="">
                    <input type ="hidden" name="recharge_number" id="recharge_number" value="">
                    <input type ="hidden" name="payment_option" value="" id="payment_option">
                    <input type="hidden" name="service_charge" id ="service_charge" value="">
                     <input type="hidden" name="service_charge_percent" id ="service_charge_percent" value="">
                      <input type="hidden" name="stv" id ="stv" value="">
                    <input type="hidden" name="total_amount" id ="total_amount" value="">
		   
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
                          
				<h4 id="mySmallModalLabel" class="modal-title">
					Order Confirmation
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-5">
						<small id="recharge_type"></small>
						<p id ="number"></p>
					</div>
					<div class="col-sm-3">
						<small>Amount</small>
						<p id ="amount"></p>
					</div>
					<div class="col-sm-4">
						<small id ="description"></small>
					</div>
                                    <div class="col-sm-4">
						<small style="display:none;" id ="service_msg"></small>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<small>Service Provider</small>
						<p id ="provider_name"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>Pay now via</small>
					</div>
					<div class="col-sm-6">
						<table>
					  	<tr>
					  		<td><span class="blk-wallet"></span></td>
					  		<td >Balance<br> <strong id="balance"><?php echo ($_SESSION["wallet_balance"]>0)?$_SESSION["wallet_balance"]:0 ?></strong></td>
					  	</tr>
					  </table>
                                           
                                            <input type="button" class="common-btn mT10 btn-block"  value="Pay with wallet" id="wallet"  onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)">
                                            <span style="color:red;" id="walletmsg"></span>
					  
					</div>
					<div class="col-sm-6">
						<table>
					  	<tr>
					  		<td><span class="blk-atm"></span></td>
					  		<td><small>Pay with your Debit or Credit Card</small></td>
					  	</tr>
					  </table>
                                            <input type="button" class="common-btn mT10 btn-block"    value="Payment Gateway" onclick="payment('recharge_amount','recharge_operator','recharge_flag','recharge_number',this.value)">
					  	
					  <!--</button>-->
					</div>
				</div>
<!--				<div class="row mT15">
					<div class="col-sm-12">
						<small>Have a coupon code?</small>
						<input type="text" class="form-control" placeholder="Redeem">
					</div>
				</div>-->
			</div>
		</div>
                                          </form>

	</div>
</div>


<!-- popup for order success -->
<div class="modal fade in ordersuccess"  id="paymentconfirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
                            <button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				
			</div>
			<div class="modal-body">
                            
                            <span style="color:red;"><b>Coming soon!!!!!!</b></span>

			</div>
		</div>
	</div>
</div>

<div class="modal fade in ordersuccess1" id="ordersuccess1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="" id="order_response"></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="order_msg"></p>
					</div>
					<div class="col-sm-5">
						<small id="order_type"></small>
						<p id ="order_number"></p>
					</div>
					<div class="col-sm-3">
						<small >Amount</small>
						<p id="order_amount"></p>
					</div>
                                    
<!--					<div class="col-sm-4">
						<small id="order_desscription">500MB 3G Plan valid for 30 days.</small>
					</div>-->
				</div>
				<div class="row">
					<div class="col-sm-5">
						<small>Service Provider</small>
						<p id="order_serviceprovider">Reliance</p>
					</div>
                                    <div class="col-sm-5">
						<small>Transaction Id</small>
						<p id="trans_id"></p>
					</div>
					<div class="col-sm-6">
						<small>Balance</small>
						<p id="closing_balanace"></p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 popupdivision">
						<small>&nbsp;</small>
					</div>
					<div class="col-sm-12 mT15" style="display:none;" id="success_msg">
						<p>If your transaction does not Succeed</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
                                       <div class="col-sm-12 mT15" style="display:none;" id="error_msg">
						<p>Kindly contact us for assistance</p>
						<button class="callaskBTN"><span class="popupicons-call"></span> Call Us</button>
						<button class="callaskBTN"><span class="popupicons-ask"></span> Ask Us</button>
						<br><br>
					</div>
                                    <div class="col-sm-12 mT15" style="display:none;"  id="order_deal">
						
						<button class="common-btn-submit"  onclick="getfreebie('ordersuccess1');"> Click to get free deals</button>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


     <div class="modal fade in congratulations" id="congratulations" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm1">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal" type="button" title="Close">
                        <img src="images/cross.png">
                    </button>
                   <form method = 'post' id='deal_form' action='wallet.php'>
                       <input type='hidden' name='lat' id='lat' value=''>
                       <input type='hidden' name='shop_id' id='shop_id' value=''>
                       <input type='hidden' id='long' name='long' value=''>
                   </form>
                    <h4 id="mySmallModalLabel" class="modal-title">
                        Congratulations
                    </h4>
                </div>
                <div class="modal-body">
                   
                    <div id="container">
			
                            <div id="slides">
                                <div class="slides_container">
                                    
                                </div>
                                <a href="#" class="prev"><img src="images/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
				<a href="#" class="next"><img src="images/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
                            </div>

				
			
		</div>
                   
                </div>

            </div>
        </div>
    </div>
     </div>

<div class="modal fade in ordersuccess2" id="ordersuccess2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				<h4 id="mySmallModalLabel" class="modal-title">
					<span class="" id="wallet_response"></span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="wallet_msg"></p>
					</div>
					
					<div class="col-sm-3">
						<small >Amount</small>
						<p id="wallet_amount"></p>
					</div>
                                    
<!--					<div class="col-sm-4">
						<small id="order_desscription">500MB 3G Plan valid for 30 days.</small>
					</div>-->
                                         <div class="col-sm-12 mT15" style="display:none;"  id="wallet_deal">
						
						<button  class="common-btn-submit" onclick="getfreebie('ordersuccess2');">Click to get free deals</button>
						
						
					</div>
				
				</div>
                            
				</div>
			</div>
		</div>
	</div>

<div class="modal fade in ordersuccess3" id="ordersuccess3" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="">Deals claimed Successfully!!!</p>
					</div>
					
					
				</div>
                            
				
				</div>
			</div>
		</div>
	</div>
    <div class="modal fade in ordersuccess4" id="ordersuccess4" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true" onclick="closepopup();">×</span>
                                        <span class="sr-only" >Close</span>
				</button>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 mT20">
						<p class="ordersucHead" id ="freebievalidate"></p>
					</div>
					
					
				</div>
                            
				
				</div>
			</div>
		</div>
	</div>
</div>
    <?php include 'footer.php'; ?>




<!-- popup for transaction faild -->


<!-- footer box -->
	

</div><!-- //shiftbox -->
</div>
<!--<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
<script src="js/slides.min.jquery.js"></script>
<script src="js/slide.js"></script>
<script type="text/javascript" src="js/touchswipe.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    var productid = "<?php echo $_POST["rec_product"];?>";
    if(productid!=''){
        var type = "<?php echo $_POST["type"]; ?>";
        var circle = "<?php echo $_POST["rec_circle"]; ?>";
        $("#"+type+"_checkplan").show();
        checkplan("getPlanDeatils",productid,circle,type);
    }
    
    var operatorvalue = "<?php echo $_POST["operator_id"] ?>";
    var operatortype = "<?php echo $_POST["operator_type"] ?>";
    if(operatorvalue!=''){
        $("#"+operatortype+"_provider").val(operatorvalue);
        getplanoperatorchange(operatortype);
    }
    
       var loc = location.search;
       if(loc!=''){
       $(".current").removeClass("current");
       var type = loc.substr(1);
       $("."+type).addClass('current');
       localStorage.setItem("rectype",type);
       }
       else{
             localStorage.setItem("rectype","mobile");
       }
        if(loc =="?mobile")
          {
              $("#dth").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $("#mobile").show();
              $("#setrechargetype").show();
          }
          else if(loc =="?dth")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#postpaid").hide();
              $("#dth").show();
              $("#setrechargetype").hide();
          }
          else if(loc =="?data")
          {
              $("#mobile").hide();
              $("#postpaid").hide();
              $("#dth").hide();
              $("#data").show();
              $("#setrechargetype").hide();
          }
          else if(loc =="?postpaid")
          {
              $("#mobile").hide();
              $("#data").hide();
              $("#dth").hide();
              $("#postpaid").show();
              $("#setrechargetype").show();
          }

	var width = $(window).width();
	var height = $(window).height(); 
	var screenTimer = null;

	function detectScreen (){
        $(window).resize(function() {
          height = $(window).height(); 
          width = $(window).width(); 
          getScreen ();
        });

        function getScreen (){
          return {'width': getWidth () };
        }
        screenTimer = setInterval(function(){ getScreen(),50});
            }

            function getWidth(){
            $(location).attr('href');
            var pathname = window.location;
        
            if (pathname=='http://pay1.my-fbapps.info/' || pathname=='http://pay1.my-fbapps.info/index.html') {
            $('.leftnavi').css('width','220px');
            $('.shiftbox').css('padding-left','220px');
            $('.leftnavi ul li:first-child').css('margin-left','-10px');
            $('.leftnavi ul li strong').css('opacity','1');
           console.log ( 'width: ' + width);
        if(width<=1024) {
            $('.shiftbox').css('padding-left','0px');
            $('.leftnavi').css('width','55px');
            }
        else{
            $('.shiftbox').css('padding-left','220px');
            $('.leftnavi').css('width','220px');
            }
            }
            }

            detectScreen ();
            
             $(".opencheckplans").click(function () {
             $(".checkplans").slideDown();
              $(".opencheckplans").prop('disabled', true);
		});

		$(".hidecheckplans").click(function(){
                $(".checkplans").slideUp();
                $(".opencheckplans").prop('disabled', false);
            });

		$(document).click(function (e) {
		    if (!$(e.target).hasClass("opencheckplans") 
		        && $(e.target).parents(".checkplans").length === 0) 
		    {
		        $(".checkplans").slideUp();
		        $(".opencheckplans").prop('disabled', false);
		        
		    }
		});
//                jQuery(".carousel-inner").carousel();
////   // Enable swiping...
//    jQuery(".carousel-inner").swipe({
//        swipeLeft:function(event, direction, distance, duration, fingerCount) {
//            alert(fingerCount);
//             alert(event);
//            jQuery(this).parent().carousel('prev');
//        },
//        swipeRight: function() {    
//            jQuery(this).parent().carousel('next');
//        },
//        threshold:0
//    });
           
            var storagedate = localStorage.getItem('datetime');
            var date = <?php echo date("Ymd"); ?>;
             var methodtype = "getMobileDetails";
             var mobileno = "all";
            if(storagedate!=date){
              $.ajax({
            url: "login.php",
            type:"POST",
            data:{
                 method : methodtype,
                 mobileno: mobileno
                 },
            dataType:"json",
            success:function(data){
              $.each(data,function(operatordatakey,operatordataval){
              localStorage.setItem(operatordatakey, JSON.stringify(operatordataval));
              localStorage.setItem("datetime",date);
              });
  
            },
            error: function (error) {
            }
            });

}

    });



      var xx = setInterval(function(){
      var currentScroll; 
      if (document.documentElement.scrollTop)
        { currentScroll = document.documentElement.scrollTop; }
      else
        { currentScroll = document.body.scrollTop; }


      if (currentScroll>='300')
      {
        $('.shiftbox').css('padding-left','0px');
            $('.leftnavi').css('width','55px');
            $('.leftnavi ul li:first-child').css('margin-left','-150px');
            $('.leftnavi ul li strong').css('opacity','0');
      }
      else
      {
        // $('#data').removeClass('hilite');
      }

       }, 100); 

       $("ul.mobile li a").click(function(e){
            e.preventDefault();
             var operatorid = $(this).attr("href");
              var id = $(this).attr("href").substr(1);
              if(id=="deals.php"){
                  window.location="deals.php";
              } else if(id=="index.php") {
                  window.location="index.php";
                }
             else{
                 window.location.hash = id;
                 localStorage.setItem("rectype",id);
               }
              if(id!="index.php"){
              $(".current").removeClass("current");
              $(this).addClass('current');
               }
          
              if(operatorid =="#mobile")
              {
                  $("#dth").hide();
                  $("#data").hide();
                  $("#postpaid").hide();
                  $("#electricity").hide();
                  $("#orderconfirmation").modal("hide");
                  $("#setrechargetype").show();
                  $(operatorid).show();
              }
              else if(operatorid =="#dth")
              {
                  $("#mobile").hide();
                  $("#data").hide();
                  $("#postpaid").hide();
                  $("#electricity").hide();
                  $("#setrechargetype").hide();
                  $("#orderconfirmation").modal("hide");
                  $(operatorid).show();
              }
              else if(operatorid =="#data")
              {
                  $("#mobile").hide();
                  $("#postpaid").hide();
                  $("#dth").hide();
                  $("#setrechargetype").hide();
                  $("#orderconfirmation").modal("hide");
                  //$(this).addClass("current");
                  $(operatorid).show();
              }
              else if(operatorid =="#postpaid")
              {
                  $("#mobile").hide();
                  $("#data").hide();
                  $("#dth").hide();
                  $("#electricity").hide();
                  $("#setrechargetype").hide();
                  //$(this).addClass("current");
                  $("#orderconfirmation").modal("hide");
                  $(operatorid).show();
              }
              else if(operatorid =="#electricity")
              { 
               $(".modal fade in orderconfirmation").modal("hide");
               
              }
        
            

        });
      
   $(document).ready(function(){
       
    
//      var latitude = "<?php echo $_SESSION["latitude"] ?>";
//      var longitude = "<?php echo $_SESSION["longitude"] ?>";
  var amount = localStorage.getItem('amount');
   var serviceurl = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/get_all_deal_data/?";
   var latitude = localStorage.getItem("latitude");
   var longitude = localStorage.getItem("longitude");
   var mob_number = "<?php echo $_SESSION["mobile"] ?>";
   var showmodal = localStorage.getItem("showmodal");
   
   console.log(latitude);
   console.log(longitude);
      
      var htmlcontent = "";
      var htmlhead = "";
      var htmladdcontent = "";
      var htmlform = "";
      var i = 1;
    
      var amountArray = new Array();
      var freebieMinValue = new Array();
      var freebieMaxValue = new Array();
     // var j = 0;
         $.ajax({
            url: serviceurl,
            type:"POST",
            data:{
                 longitude: longitude,
                 latitude : latitude,
                 mobile:mob_number,
                 res_format : "jsonp"
                 },
          
            timeout: 50000,
            dataType: "jsonp",
            crossDomain: true,
            success:function(dealsdata){
              if(dealsdata.status=="success") { 
                 $.each(dealsdata.description,function(dealkey,dealvalue){
                     if(dealvalue.offer_detail.allow>0){
                     if(parseInt(dealvalue.min_amount)<=parseInt(amount)){
                         freebieMinValue.push(dealvalue);
                     }
                     else {
                          freebieMaxValue.push(dealvalue);
                     }
                      }
                 });
                 var dealsdata = freebieMinValue.concat(freebieMaxValue);
             }
                  $.each(dealsdata,function(key,value){
                       if(i==1){
                           var classname ="item active";
                           var nstyle = "";
                            } else{
                                   var classname ="item";
                                    var nstyle = "";
                                }   
                            var addclass = "col-sm-6 row_left";
                            var mapclass = "col-sm-3 row_right";
                            var dealname  = "\'" + value.offer_detail.name + "\'";
                            var deal_id  = "\'" + value.id + "\'";
                            var offer_id  = "\'" + value.offer_detail.id + "\'";
                            var minamount = "\'" + value.min_amount + "\'";
                                htmlcontent+="<div class='slide'><div><h2>"+value.offer_detail.name+"</h2><p>"+value.offer_detail.short_desc+"</p><img  src='"+value.img_url+"' height='300px;' width='300px;'> <p style='align:center;'>"+i+"of "+dealsdata.length+"</p></div>";
                                htmlcontent+="<div><h2>"+value.dealname+"</h2><p>"+value.content_txt.replace('&lt','<').replace('&gt', '>')+"</p></div>";
                                htmlcontent+="<div><div class='"+addclass+"'>"
                                htmlcontent+="<h3>"+value.location_detail[0].address+"</h3>"
                                if(parseInt(amount)>=parseInt(value.min_amount)){
                                htmlcontent+='<a href="javascript:claimdeal('+deal_id+','+offer_id+')">Claim this free gift</a>'
                                } else {
                                 htmlcontent+='<a href="javascript:validatefreebie('+dealname+','+minamount+')">not eligible</a>'
                                }
                                htmlcontent+="</div>";
                                 htmlcontent+="<div class='"+mapclass+"'>"
                                htmlcontent+='<a href="javascript:getdeallocation('+value.latitude+','+value.longitude+','+deal_id+')"><img  src="images/map-icon.png"></a>'
                                if(latitude!= null && longitude!= null){
                                htmlcontent+="<h3>"+Number(value.distance).toFixed(2)+"</h3>"
                                if(value.distance>1){
                                htmlcontent+="<span>(Km)</span>"
                               } else {
                                htmlcontent+="<span>(m)</span>"
                                }
                                }   
                                htmlcontent+="</div></div></div>";
                                   
                              i++;
                              });
                              
                            $(".slides_container").append(htmlcontent);
                             //console.log(htmlform);
                              if(showmodal=="true"){
                               $('#congratulations').modal('toggle');
                                localStorage.removeItem("showmodal");
                                 slideimage();
                              }
                        },
                 
            error: function (error) {
              console.log(error);
            }
            });
          var mobileno = "<?php echo $_SESSION["mobile"]; ?>";
//        var dealtime = localStorage.getItem("dealtime_"+mobileno);
//        if(dealtime!=null){
//        var minutes = 1000 * 60;
//        var difference =new Date().getTime()-dealtime;
//        var timedifference = Math.round(difference / minutes);
//        }
//        else {
//            var timedifference = "";
//        }
//        
     
        var paymentstatus = localStorage.getItem("paymentstatus");
        var walletstatus = localStorage.getItem("walletstatus");
        var provider =  localStorage.getItem('provider');
        var number = localStorage.getItem('number');
        var type = localStorage.getItem('type');
        var balance = "<?php echo $_SESSION["wallet_balance"] ?>";
        var x;
        var transid = localStorage.getItem('trans_id');
//        var flag = "";
//        var checkamount =  localStorage.getItem("amountarray");
//        if(checkamount!=null){
//        var splitamount = checkamount.split(",");
//        if(splitamount.length>0){
//            for(x in splitamount)
//            {
//               if(amount>=splitamount[x])
//               {
//                   flag = "true";
//               }
//            }
//         }
//     }

     //alert(type);
     $("#order_type").html(type); 
     $("#order_number").html(number);
     $("#order_amount").html(amount);
     $("#order_serviceprovider").html(provider);
     $("#closing_balanace").html(balance);
     $("#wallet_amount").html(balance);
     $("#trans_id").html(transid);
     
//     if(showmodal=="true"){
//          $('#congratulations').modal('toggle');
//           localStorage.removeItem("showmodal");
//     }

    if(paymentstatus=="success" && walletstatus!='true' ){
     $("#order_msg").html("Transaction completed succcessfully!");
     $("#order_response").html("Success!");
     $("#order_msg").addClass("ordersucHead");
     $("#order_response").addClass("ordersucHead");
      if(type!='Postpaid Mobile Number'){
       $("#order_deal").show();
      }
//     } else if(dealtime==null&& flag=='true')
//     {
//          $("#order_deal").show();
//     }
    $("#success_msg").show();
    $('#ordersuccess1').modal('toggle' );
    localStorage.removeItem("paymentstatus");
    }
    else if(paymentstatus=="error" && walletstatus!='true')
    {
     $("#order_msg").html("Transaction failed!!!");
     $("#order_response").html("failed!");
     $("#order_msg").addClass("transfaildTXT");
     $("#order_response").addClass("transfaildTXT");
     $("#error_msg").show();
    $('#ordersuccess1').modal( 'toggle' );
    localStorage.removeItem("paymentstatus");
    }
    else if(paymentstatus=="success"  && walletstatus=="true")
    {
     $("#wallet_msg").html("wallet recharge succcessfully!!!");
     $("#wallet_response").html("Success!");
     $("#wallet_msg").addClass("ordersucHead");
     $("#wallet_response").addClass("ordersucHead");
    // if(flag=='true'){
         
       // $("#wallet_deal").show();
   //  }
//     else if(dealtime==null && flag=='true')
//     {
//         $("#wallet_deal").show();
//     }
        $('#ordersuccess2').modal( 'toggle' );
        localStorage.removeItem("paymentstatus");
        localStorage.removeItem("walletstatus");
    }
        else if(paymentstatus=="error"  && walletstatus=="true")
        {
         $("#wallet_msg").html("wallet recharge failed!!!");
         $("#wallet_response").html("failed!");
         $("#wallet_msg").addClass("transfaildTXT");
         $("#wallet_response").addClass("transfaildTXT");
        $('#ordersuccess2').modal( 'toggle' );
        localStorage.removeItem("paymentstatus");
        localStorage.removeItem("walletstatus");
        }
      var url = "<?php echo CDEV_URL; ?>/index.php/api_new/action/api/true/actiontype/check_bal";
      var html = "";
       $.ajax({
            url: url,
            type:"GET",
            data:{
                  res_format : "jsonp"
                 },
            timeout: 50000,
            dataType: "jsonp",
            jsonpCallback: 'callback',
            crossDomain: true,
            success:function(data){
           if(data.status=="success"){
            var walletbal = data.description.account_balance;
            var sessionwalletbal = "<?php echo $_SESSION["wallet_balance"]; ?>";
            if(walletbal!=sessionwalletbal) {
            $.ajax({
            url: "info.php",
            type:"POST",
            data:{
                 walletbal :walletbal,
                 },
            timeout: 50000,
            dataType: "json",
            success:function(data){
              if(data.status=="success")
              {
              location.reload();
              }
            },
            error: function (error) {
            }
            });
            }
            }
            }

            });


   
       
});
function isNumberKey(evt){
         var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }
     
     function setRechargeType(type){
         
         if(type=="Postpaid"){
             $("#mobile").hide();
             $("#postpaid").show();
         } else if(type=="Prepaid"){
             $("#mobile").show();
             $("#postpaid").hide();
         }
         
     }
     
     function validatefreebie(dealname,amount)
     {
         alert("Recharge For Rs. " +amount+" to get " +dealname);
//         $('#ordersuccess4').modal('toggle');
         // $('#congratulations').modal('hide');
         // $('#congratulations').modal('toggle');
     }

 



</script>


</body>
</html>