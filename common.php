<?php

require_once 'function.php';
require_once 'config.php';

$getcircle = file_get_contents("circle.json");
$getcircle = json_decode($getcircle);
$getcircle = objectToArray($getcircle);



$service_url = CDEV_URL . "/index.php/api_new/action/api/true/actiontype/get_all_operators/?";
$curl_response = getCurlRespose($service_url);
$operatorData = objectToArray($curl_response);
$arrayval = array();
foreach ($operatorData as $operatorkey => $operatorval) {
    if (is_array($operatorval)) {
        foreach ($operatorval as $operatorkey => $operatorvalue) {
            if ($operatorkey == "mobile") {
                $arrayval["Mobile"] = $operatorvalue;
            } elseif ($operatorkey == "data") {
                $arrayval["data"] = $operatorvalue;
            } elseif ($operatorkey == "dth") {
                $arrayval["dth"] = $operatorvalue;
            } elseif ($operatorkey == "postpaid") {
                $arrayval["postpaid"] = $operatorvalue;
            }
        }
    }
}


$operatorarray = array();
$datalimit = array();
foreach ($arrayval["data"] as $key => $val) {
    $operatorarray[$val["opr_code"]] = $val["id"];
    $datalimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
}

$data = json_encode($operatorarray);
$datalimit = json_encode($datalimit);

$postpaidoperatorarray = array();
$postpaidlimit = array();

foreach ($arrayval["postpaid"] as $key => $val) {
    $postpaidoperatorarray[$val["opr_code"]] = $val["id"];
    $postpaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["service_tax_percent"], $val["product_id"]);
}

$postpaiddata = json_encode($postpaidoperatorarray);
$postpaidlimit = json_encode($postpaidlimit);

$prepaidoperatorarray = array();
$prepaidlimit = array();

foreach ($arrayval["Mobile"] as $key => $val) {
    $prepaidoperatorarray[$val["opr_code"]] = $val["id"];
    $prepaidlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
}

$prepaiddata = json_encode($prepaidoperatorarray);
$prepaidlimit = json_encode($prepaidlimit);

$dthoperatorarray = array();
$dthlimit = array();
foreach ($arrayval["dth"] as $key => $val) {
    $dthoperatorarray[$val["opr_code"]] = $val["id"];
    $dthlimit[$val["opr_code"]] = array($val["max"], $val["min"], $val["stv"], $val["charges_slab"], $val["product_id"]);
}
$dthdata = json_encode($dthoperatorarray);
$dthlimit = json_encode($dthlimit);
?>

