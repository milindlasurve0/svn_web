<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html lang="en">
<head>
	<title>Login || PAY1</title>
	<meta charset="utf-8">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
        <link rel="shortcut icon" type="image/png" href="/images/pay1.png"/>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="logsignBODY">
     
<div>
<!-- navigation -->
<div class="mobilemenu">
 	<p class="mobiletoggle" data-idd="#mobnav"></p>
	<a href="index1.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
</div>
<div class="opacitybg"></div>
<div class="leftnavi" id="mobnav">
	<ul>
		<li>
			<a href="index.php?mobile"><img src="images/logo.jpg"></a>
			<strong>Recharge</strong>
		</li>
                <li><a href="index.php?mobile"  class="current"><span class="customicon-mobile"></span> Mobile</a></li>
		<li><a href="index.php?dth"><span class="customicon-dth"></span> DTH</a></li>
		<li><a href="index.php?data"><span class="customicon-dcable"></span> Data Card</a></li>
		<li class="freespace">&nbsp;</li>
		<li><a href="deals.php"><span class="customicon-deals"></span>Deals</a></li>
<!--		<li class="bill freespace"><small>Bill</small></li>
		<li><a href="index.php?postpaid"><span class="customicon-pmobile"></span> Postpaid mobile</a></li>-->
<!--		<li><a href="#electricity" data-target=".ordersuccess" data-toggle="modal"><span class="customicon-Electricity"></span> Electricity</a></li>-->
	</ul>
</div>

                      <div class="modal fade in ordersuccess"  id="paymentconfirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" type="button" title="Close">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				
			</div>
			<div class="modal-body">
                            
                         <span style="color:red;"><b>Coming soon!!!!!!</b></span>
			</div>
		</div>
	</div>
</div>