<?php
require_once 'function.php';
session_start();
//ini_set("display_errors","on");
error_reporting(E_ALL);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["mob-number"])) {
        $mobErr = "Mobile Number is required";
    } else {
        $mobnumber = test_input($_POST["mob-number"]);
    }

    if (empty($_POST["password"])) {
        $passErr = "Password is required";
    } else {
        $password = test_input($_POST["password"]);
    }
    
    $data = array("username"=>$mobnumber,"password"=>$password,"uuid"=>"","latitude"=>"","logitude"=>"","device_type"=>"");


  if(!empty($mobnumber) && !empty($password)){
  $service_url = "http://cdev.pay1.in/index.php/api_new/action/actiontype/signin/api/true/?";
  $curl_response = getCurlRespose($service_url,$data);
  
//  echo "<pre>";
//  print_r($curl_response);die;
  $status = $curl_response->status;
  
if ($status  == 'failure') {
   $loginErr = "Incorrect username or password";
    } else{
        $_SESSION["name"] = $curl_response->description->name;
        $_SESSION["mobile"] = $curl_response->description->mobile;
        $_SESSION["email"] = $curl_response->description->email;
        $_SESSION["gender"] = $curl_response->description->gender;
        $_SESSION["date_of_birth"] = $curl_response->description->date_of_birth;
        $_SESSION["user_id"] = $curl_response->description->user_id;
        $_SESSION["gcm_reg_id"] = $curl_response->description->gcm_reg_id;
        $_SESSION["uuid"] = $curl_response->description->uuid;
        $_SESSION["longitude"] = $curl_response->description->longitude;
        $_SESSION["latitude"] = $curl_response->description->latitude;
        $_SESSION["device_type"] = $curl_response->description->description->device_type;
        $_SESSION["wallet_balance"] = $curl_response->description->wallet_balance;
        $_SESSION["service_charge_percent"] = $curl_response->description->service_charge_percent;
        $_SESSION["service_tax_percent"] = $curl_response->description->service_tax_percent;
        $_SESSION["support_number"] = $curl_response->description->support_number;
        header("location:index.php");
        
    }
}
}

?>

<!DOCTYPE html>
<!--<html lang="en">
<head>
	<title>Login || PAY1</title>
	<meta charset="utf-8">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/mobile-tablet-style.css">
	 HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries 
   WARNING: Respond.js doesn't work if you view the page via file:// 
  [if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]
</head>
<body class="logsignBODY">
     
<div>
 navigation 
<div class="mobilemenu">
 	<p class="mobiletoggle" data-idd="#mobnav"></p>
	<a href="index.html" title="Pay1"><img src="images/logo.jpg" align="logo-image"></a>
</div>
<div class="opacitybg"></div>
<div class="leftnavi" id="mobnav">
	<ul>
		<li>
			<a href="index.html"><img src="images/logo.jpg"></a>
			<strong>Recharge</strong>
		</li>
		<li><a href="index.html"><span class="customicon-mobile"></span> Mobile</a></li>
		<li><a href="#"><span class="customicon-dth"></span> DTH</a></li>
		<li><a href="#"><span class="customicon-dcable"></span> Data Card</a></li>
		<li class="freespace">&nbsp;</li>
		<li><a href="deals.html"><span class="customicon-deals"></span>Deals</a></li>
		<li class="bill freespace"><small>Bill</small></li>
		<li><a href="#"><span class="customicon-pmobile"></span> Postpaid mobile</a></li>
		<li><a href="#"><span class="customicon-Electricity"></span> Electricity</a></li>
	</ul>
</div>-->
	<!-- //close navigation -->
        <?php        include 'left.php';?>
       
<div class="shiftbox">
	<div class="hutpart" style="height:100%;width:100%; display:block; position:absolute;">
		<div class="container">
			<div class="row mT20">
				<div class="col-md-6 col-md-offset-3">
					<div class="signlogBOX">
                                            <span style="color:red;"><?php echo $loginErr;?></span>
<!--						<span class="btn-info">Login</span>-->
<!--						<form class="form mT20" role="form">-->
                                                    <form method="post" class="form mT20" id ="target" action="login.php">
						  <div class="form-group">
						    <label class="" for="">Mobile Number</label>
						    <div class="input-group">
						      <div class="input-group-addon">
						      	<span class="loginsign-mobile"></span>
						      </div>
						      <input class="form-control" onkeypress="return isNumberKey(event)" id ="mob-number" name="mob-number" type="text" maxlength="10">
						    </div>
                                                    <span style="color:red;"> <?php echo $mobErr;?></span>
                                                    <br>
						  </div>
						  <div class="form-group">
						  	<label class="" for="exampleInputEmail2">Pin</label>
						    <div class="input-group">
						      <div class="input-group-addon">
						      	<span class="loginsign-lock"></span>
						      </div>
						      <input class="form-control" name ="password" id ="password" type="password">
						    </div>
                                                        <span style="color:red;"><?php echo $passErr;?></span>
                                                    <br>
						    <div class="text-right">Forgot pin</div>
						  </div>
						  <div class="form-group">
						  	<button type="button" id="login" class="btn btn-primary btn-lg btn-block">Login</button>
						  	<p class="logOR">Or</p>
								<a href="sign-up.html" class="btn btn-default btn-lg btn-block">Sign up</a>
						  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- //shiftbox -->
        <!--</form>-->
</div>
<script src="js/jquery.min.2.1.1.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script>
    $('#login').click(function(){
        var mobileNo = $("#mob-number").val();
        var password = $("#password").val();
        if(mobileNo=='')
        {
            alert("Please enter a valid mobile no.");
            return false;
        }
        else if(password=='')
        {
            alert("Password can not be blank");
            return false;
        }
        else
        {
            $("#target").submit();
        }

  
});


function isNumberKey(evt){
 var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true
     }


</script>



</body>
</html>